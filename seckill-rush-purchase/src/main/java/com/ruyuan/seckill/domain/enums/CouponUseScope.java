package com.ruyuan.seckill.domain.enums;

/**
 * 优惠券使用范围
 *
 */
public enum CouponUseScope {

    /**
     * 全部商品
     */
    ALL,

    /**
     * 某分类
     */
    CATEGORY,

    /**
     * 部分商品
     */
    SOME_GOODS;

}
