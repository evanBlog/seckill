package com.ruyuan.seckill.service;


import com.ruyuan.seckill.domain.vo.GoodsQuantityVO;

import java.util.List;


/**
 * 商品库存接口
 *
 * 内部实现为redis +lua 保证原子性
 */
public interface GoodsQuantityManager {


    /**
     * 秒杀库存更新接口
     *
     * @param goodsQuantityList 要更新的库存vo List
     * @return 如果更新成功返回真，否则返回假
     */
    Boolean updateSeckillSkuQuantity(List<GoodsQuantityVO> goodsQuantityList);


}
