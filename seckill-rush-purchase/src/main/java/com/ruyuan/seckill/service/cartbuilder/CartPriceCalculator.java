package com.ruyuan.seckill.service.cartbuilder;


import com.ruyuan.seckill.domain.Buyer;
import com.ruyuan.seckill.domain.vo.CartVO;
import com.ruyuan.seckill.domain.vo.PriceDetailVO;

import java.util.List;

/**
 * 购物车价格计算器
 */
public interface CartPriceCalculator {

    /**
     * 计算购物车价格
     * @param cartList  购物车列表
     * @param includeCoupon  是否包含优惠券
     * @return  PriceDetailVO 购物车价格对象
     */
    PriceDetailVO countPrice(List<CartVO> cartList, Boolean includeCoupon);
    PriceDetailVO countPrice(List<CartVO> cartList, Buyer buyer, Boolean includeCoupon);


}
