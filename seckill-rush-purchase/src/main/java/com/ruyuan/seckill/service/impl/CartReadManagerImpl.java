package com.ruyuan.seckill.service.impl;


import com.ruyuan.seckill.domain.Buyer;
import com.ruyuan.seckill.domain.enums.CartType;
import com.ruyuan.seckill.domain.enums.CheckedWay;
import com.ruyuan.seckill.domain.vo.CartSkuVO;
import com.ruyuan.seckill.domain.vo.CartVO;
import com.ruyuan.seckill.domain.vo.CartView;
import com.ruyuan.seckill.service.CartReadManager;
import com.ruyuan.seckill.service.cartbuilder.*;
import com.ruyuan.seckill.service.cartbuilder.impl.DefaultCartBuilder;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * 购物车只读操作业务类
 */
@Service
@Slf4j
public class CartReadManagerImpl implements CartReadManager {




    /**
     * 购物车促销渲染器
     */
    @Autowired
    private CartPromotionRenderer cartPromotionRenderer;

    /**
     * 购物车价格计算器
     */
    @Autowired
    private CartPriceCalculator cartPriceCalculator;

    /**
     * 购物车sku数据渲染器
     */
    @Autowired
    private CartSkuRenderer cartSkuRenderer;

    /**
     * 数据校验
     */
    @Autowired
    private CheckDataRebderer checkDataRebderer;

    /**
     * 购物车优惠券渲染器
     */
    @Autowired
    private CartCouponRenderer cartCouponRenderer;

    /**
     * 购物车运费价格计算器
     */
    @Autowired
    private CartShipPriceCalculator cartShipPriceCalculator;


    @Override
    public CartView getCartListAndCountPrice(CheckedWay way) {

        //调用CartView生产流程线进行生产
        CartBuilder cartBuilder = new DefaultCartBuilder(CartType.CART, cartSkuRenderer, cartPromotionRenderer, cartPriceCalculator, checkDataRebderer);

        /**
         * 生产流程：渲染sku->校验sku是否有效->渲染促销规则->计算价格->生成成品
         * 生产流程说明 ： 校验sku是否有效必须放在渲染促销规则之前，如果参加满减活动的商品失效，那么满减活动无需渲染
         * update by liuyulei 2019-05-17
         */
        CartView cartView = cartBuilder.renderSku(way).checkData().renderPromotion().countPrice(false).build();
        List<CartVO> itemList = cartView.getCartList();
        processChecked(itemList);
        return cartView;
    }


    /**
     * 处理购物车的选中情况
     * 根据每个商品的选中情况来 设置店铺是否全选
     *
     * @param itemList
     */
    private void processChecked(List<CartVO> itemList) {
        for (CartVO cartVO : itemList) {
            //设置默认为店铺商品全选
            cartVO.setChecked(1);
            cartVO.setInvalid(0);

            //如果购物车有一个有效的商品
            Boolean notInvalid = false;

            for (CartSkuVO skuVO : cartVO.getSkuList()) {
                // 如果商品没有选中 并且他不是一个有效的商品
                if (skuVO.getChecked() == 0 && skuVO.getInvalid() == 0) {
                    cartVO.setChecked(0);
                }
                if (skuVO.getInvalid() == 0) {
                    notInvalid = true;
                }
            }

            //如果 所有商品都无效 && 购物车状态为以选中
            if (!notInvalid && cartVO.getChecked() == 1) {
                cartVO.setInvalid(1);
            }
           log.info("购物车选中处理结果:{}",cartVO.toString());
        }
    }


    @Override
    public CartView getCheckedItems(CheckedWay way, Buyer buyer) {

        // 调用CartView生产流程线进行生产
        CartBuilder cartBuilder = new DefaultCartBuilder(CartType.CHECKOUT, cartSkuRenderer, cartPromotionRenderer,
                cartPriceCalculator, cartCouponRenderer, cartShipPriceCalculator, checkDataRebderer, buyer);

        /**
         * 生产流程：渲染sku->校验sku是否有效->渲染促销规则(计算优惠券）->计算运费->计算价格 -> 渲染优惠券 ->生成成品
         * 生产流程说明 ： 校验sku是否有效必须放在渲染促销规则之前，如果参加满减活动的商品失效，那么满减活动无需渲染
         */
        CartView cartView = cartBuilder
                                        // 从redis缓存中获取当前用户购买的商品的列表
                                       .renderSku(way)
                                        // 从上一步中获取到的购物车商品SKU列表中 过滤掉购物车中的商品SKU数据以及过期的数据 以及 可销售库存足的数据
                                       .checkData()
                                        // 获取购物车中商品的促销活动信息
                                       .renderPromotion()
                                        // 计算运费
                                       .countShipPrice()
                                        //计算总价格
                                       .countPrice(true)
                                        // 填充优惠券
                                       .renderCoupon()
                                        // 构造购物车数据
                                       .build();
        List<CartVO> cartList = cartView.getCartList();


        // 无效的购物车清
        List<CartVO> invalidCart = new ArrayList<>();

        for (CartVO cart : cartList) {
            List<CartSkuVO> skuList = cart.getSkuList();
            List<CartSkuVO> newSkuList = new ArrayList<CartSkuVO>();

            for (CartSkuVO skuVO : skuList) {
                //只将选中的压入
                if (skuVO.getChecked() == 1) {
                    newSkuList.add(skuVO);
                }
            }
            cart.setSkuList(newSkuList);

            if (newSkuList.size() == 0) {
                invalidCart.add(cart);
            }
        }
        // 去除没有可以购买商品的购物车
        for (CartVO cart : invalidCart) {
            cartList.remove(cart);
        }

        return cartView;

    }


}
